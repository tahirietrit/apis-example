package callbacks;

/**
 * Created by macb on 06/03/17.
 */

public interface DitetCallback {
    void onDitetResponse(String ditetResponse);
    void onDFilmsResponse(String ditetResponse);
}
