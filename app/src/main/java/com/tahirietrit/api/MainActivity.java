package com.tahirietrit.api;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import org.json.JSONException;

import java.util.ArrayList;

import adapter.DitetAdapter;
import api.DitetAsyncTask;
import api.Endpoints;
import api.FilmatAsyncTask;
import callbacks.DitetCallback;
import model.Dita;
import model.DitetResponse;

public class MainActivity extends AppCompatActivity implements DitetCallback {

    ArrayList<Dita> ditetArraylist = new ArrayList<>();
    ListView ditetListView;
    DitetAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new DitetAsyncTask(this).execute(Endpoints.DITET_ENDPOINT);
        ditetListView = (ListView) findViewById(R.id.ditet_listview);
        adapter = new DitetAdapter(getLayoutInflater());
        ditetListView.setAdapter(adapter);
    }

    @Override
    public void onDitetResponse(String ditetResponse) {

        try {
            DitetResponse response = new DitetResponse(ditetResponse);
            ditetArraylist = response.getDitet();
            adapter.setDitetArralist(ditetArraylist);
            new FilmatAsyncTask(this).execute(Endpoints.filmsEndpoint(ditetArraylist.get(1).getDate()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDFilmsResponse(String ditetResponse) {
        System.out.println("films response "+ ditetResponse);
    }
}
