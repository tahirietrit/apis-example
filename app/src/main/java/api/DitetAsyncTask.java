package api;

import android.os.AsyncTask;

import java.io.IOException;

import callbacks.DitetCallback;

/**
 * Created by macb on 06/03/17.
 */

public class DitetAsyncTask  extends AsyncTask<String, String, String >{
    public DitetCallback callback;

    public DitetAsyncTask(DitetCallback callback){
        this.callback = callback;
    }
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            return ApiService.get(strings[0]);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        callback.onDitetResponse(s);
    }
}
