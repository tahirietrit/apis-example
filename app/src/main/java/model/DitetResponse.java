package model;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by macb on 06/03/17.
 */

public class DitetResponse {

    ArrayList<Dita> ditet = new ArrayList<>();

    public DitetResponse(String s) throws JSONException {
        JSONArray jsonArray = new JSONArray(s);
        for (int i = 0; i < jsonArray.length(); i++) {
            ditet.add(new Dita(jsonArray.optJSONObject(i)));
        }
    }

    public ArrayList<Dita> getDitet() {
        return ditet;
    }
}
