package model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by macb on 06/03/17.
 */

public class Dita {
    String day;
    String date;

    public Dita(JSONObject jsonObject) throws JSONException {
        day = jsonObject.optString("day");
        date = jsonObject.optString("date");
    }

    public String getDate() {
        return date;
    }

    public String getDay() {
        return day;
    }
}
