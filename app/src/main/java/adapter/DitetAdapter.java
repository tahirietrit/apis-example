package adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.tahirietrit.api.R;

import java.util.ArrayList;

import model.Dita;

/**
 * Created by macb on 20/03/17.
 */

public class DitetAdapter extends BaseAdapter {
    ArrayList<Dita> ditetArralist = new ArrayList<>();
    LayoutInflater inflater;
    public DitetAdapter(LayoutInflater inflater){
        this.inflater = inflater;
    }

    class ViewHolder {
        TextView ditaTextView;
        TextView dataTextView;
        public ViewHolder(View v){
            ditaTextView = (TextView) v.findViewById(R.id.dita_textview);
            dataTextView = (TextView) v.findViewById(R.id.data_textview);
        }
    }
    @Override
    public int getCount() {
        return ditetArralist.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if(view== null){
            view = inflater.inflate(R.layout.list_item, viewGroup, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }else{
            holder = (ViewHolder) view.getTag();
        }
        holder.ditaTextView.setText(ditetArralist.get(i).getDay());
        holder.dataTextView.setText(ditetArralist.get(i).getDate());
        return view;
    }

    public void setDitetArralist(ArrayList<Dita> ditetArralist) {
        this.ditetArralist = ditetArralist;
        notifyDataSetChanged();
    }
}
